const https = require('https')

const request = (options, data, ignoreParseError) =>
  new Promise((resolve, reject) => {
    const req = https.request({
      hostname: 'gitlab.com',
      headers: {
        'Content-Type': 'application/json',
        'Private-Token': process.env.PRIVATE_TOKEN
      },
      ...options
    }, res => {
      const chunks = []
      res.on('data', chunk => chunks.push(chunk))
      res.on('error', error => reject(JSON.parse(error)))
      res.on('end', () => {
        let jsonResponse = {}
        const rawResponse = chunks && chunks.length && Buffer.concat(chunks).toString()

        try {
          jsonResponse = JSON.parse(rawResponse)
        } catch (error) {
          jsonResponse = {
            rawResponse
          }
          !ignoreParseError && console.log(error)
        }

        resolve(jsonResponse)
      })
    })

    if (data) {
      req.useChunkedEncodingByDefault = true
    }

    req.on('error', reject)
    req.end(data && JSON.stringify(data))
  })

const getPackage = (name, version) =>
  request({
    method: 'GET',
    path: `/api/v4/projects/${process.env.CI_PROJECT_ID}/packages?package_name=${name}&order_by=version&sort=desc`
  }).then((res = []) => res.find(tag => tag.version === version)).catch(error => console.error(error) && false)

const deletePackage = id =>
  request({
    method: 'DELETE',
    path: `/api/v4/projects/${process.env.CI_PROJECT_ID}/packages/${id}`
  })
    .then(res => res)
    .catch(error => console.error(error) && false)

;(async () => {
  const CURRENT_TAG = '@republish-issue/my-package@1.0.0'
  const [, name, version] = CURRENT_TAG.split('@')
  const pkg = await getPackage(name, version)
  if (pkg) {
    await deletePackage(pkg.id)
    console.log(`npm package ${CURRENT_TAG} unpublished`)
  } else {
    console.log(`npm package ${CURRENT_TAG} not found. nothing to unpublish.`)
  }
})()
